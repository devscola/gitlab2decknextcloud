# GitLabBoard2NextCloudDeck

## API Gitlab

https://docs.gitlab.com/ee/api/issues.html

### Personal access tokens
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

Es necesario **token** para obtener las notas, pero para Projects e Issues públicos (como es el caso de Devscola) no es necesario.

### Peticiones

#### GET projects
https://gitlab.com/api/v4/projects

#### GET issues
https://gitlab.com/api/v4/projects/<boardId>/issues?per_page=1000

#### GET notes
https://gitlab.com/api/v4/projects/<boardId>/issues/<cardId>/notes

Cada Issue tiene **notes** que pueden tener contenido o no.
Es necesario usar el **token**.

## API Deck
https://deck.readthedocs.io/en/latest/API/

### Acceso
Se accede con **Basic Authentication** (usuario:contraseña).

### Peticiones

#### GET Stacks
https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/{boardId}/stacks

#### GET Cards
https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/{boardId}/stacks/{stackId}/cards/{cardId}

#### POST Cards
https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/{boardId}/stacks/{stackId}/cards

Content-Type: application/json

Body JSON (necesita, al menos, el **title**):
```
{
	"title": "Prueba desde API"
}
```

#### PUT Cards
https://cloud.devscola.org/index.php/apps/deck/api/v1.0/boards/{boardId}/stacks/{stackId}/cards/{cardId}

Content-Type: application/json

Body JSON (necesita, **type**, **owner**):
```
{
	"title": "Prueba desde API-II",
	"description": "Texto largo de prueba.\n\nTexto largo de prueba",
	"type": "plain",
	"duedate": "2020-06-10T00:00:00+00:00",
  "owner": {
    "primaryKey": "devsputnik",
    "uid": "devsputnik",
    "displayname": "Devsputnik",
    "type": 0
  }
}
```

## Problemas CORS navegador

Para evitar el error de Cors utilizar el siguiente plugin para chrome
https://chrome.google.com/webstore/detail/cors-unblock/lfhmikememgdcahcdlaciloancbhjino
